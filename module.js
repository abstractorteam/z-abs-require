
'use strict';

const Fs = require('fs');
const Path = require('path');
let Module = require('module');
const originalRequire = Module.prototype.require;


class AbsModule {
  static calculateDynamicConfig() {
    let nextPropertyName = '';
    let nextProperyValueMulti = '';
    const parameters = new Map();
    const tasks = [];
    
    const argv = process.argv.slice(2);
    argv.forEach((arg) => {
      if(0 === nextPropertyName.length) {
        if(arg.startsWith('--')) {
          nextPropertyName = arg.substring(2);
        }
        else {
          tasks.push(arg);
        }
      }
      else if(0 !== nextProperyValueMulti.length) {
        if(arg.endsWith('"')) {
          nextProperyValueMulti += arg.substring(0, arg.length - 1);
          tasks.push(nextProperyValueMulti);
          nextProperyValueMulti = '';
        }
        else {
          nextProperyValueMulti += arg;
        }
      }
      else {
        if(arg.startsWith('"')) {
          nextProperyValueMulti = arg.substring(1);
        }
        else if(arg.startsWith('--')) {
          parameters.set(nextPropertyName, undefined);
          nextPropertyName = arg.substring(2);
        }
        else {
          parameters.set(nextPropertyName, arg);
          nextPropertyName = '';
        }
      }
    });
    if(0 !== nextPropertyName.length) {
      parameters.set(nextPropertyName, undefined);
    }
    return {
      app: '',
      parameters: parameters,
      tasks: tasks
    };
  }
  
  static init(dynamicConfig = null, paths = '', devInstalled = false) {
    if(!devInstalled) {
      paths = [`${Path.resolve('.')}/dist/Layers/`];
      const delimiter = 'win32' === process.platform ? ';' : ':';
      if(undefined !== process.env.NODE_PATH) {
        process.env.NODE_PATH = `${process.env.NODE_PATH}${delimiter}${paths.join(delimiter)}`;
      }
      else {
        process.env.NODE_PATH = `${paths.join(delimiter)}`;
      }
      require('module').Module._initPaths();
    }
    if(!dynamicConfig) {
      dynamicConfig = AbsModule.calculateDynamicConfig();
      Reflect.set(global, 'dynamicConfig@abstractor', dynamicConfig);
      const parameters = [];
      dynamicConfig.parameters.forEach((value, key) => {
        parameters.push([key, value]);
      });
      const dynamicConfigStringified = JSON.stringify({
        parameters: parameters,
        tasks: dynamicConfig.tasks
      }).replace(/,/g, ';');
      Reflect.set(global, 'dynamicConfigStringified@abstractor', dynamicConfigStringified);
    }
    if(dynamicConfig.parameters.has('dev')) {
      if(!devInstalled && Fs.existsSync('../z-abs-require')) {
        const absModule = require(`${Path.resolve('../z-abs-require/module')}`);
        return absModule.init(dynamicConfig, paths, true);
      }
      else {
        Module.prototype.require = function() {
          if(arguments[0].startsWith('z-abs-')) {
            if(arguments[0].startsWith('z-abs-local-server')) {
              const requirePath = `../${arguments[0]}`;
              if(Fs.existsSync('../z-abs-local-server')) {
                arguments[0] = requirePath;
              }
            }
            else if(arguments[0].startsWith('z-abs-project')) {
              const requirePath = `../${arguments[0]}`;
              if(Fs.existsSync('../z-abs-project')) {
                arguments[0] = requirePath;
              }
            }
            else {
              arguments[0] = `${paths}/${arguments[0]}`;
            }
          }
          return originalRequire.apply(this, arguments);
        }
      }
    }
    else {
      Module.prototype.require = function() {
        if(arguments[0].startsWith('z-abs-')) {
          if(arguments[0].startsWith('z-abs-local-server')) {}
          else if(arguments[0].startsWith('z-abs-project')) {}
          else {
            arguments[0] = `${Path.resolve('.')}/dist/Layers/${arguments[0]}`;
          }
        }
        return originalRequire.apply(this, arguments);
      }
    }
    return dynamicConfig;
  }
}

module.exports = AbsModule;
